class DogBreedFetcher
  attr_reader :breed

  def initialize(name=nil)
    @name  = breed || "random"
    @breed = Breed.find_or_initialize_by(name: name)
  end

  def fetch
    return @breed if @breed.pic_url.present?

    @breed.pic_url = fetch_info["message"]
    @breed.save && @breed
  end

  def self.fetch_all
    breeds_list = []
    breeds = JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list/all"))['message']
    breeds.each do |breed|
      name = breed[0].capitalize
      value = breed[0]
      if breed[1].length > 0
        breed[1].each do |item|
          breeds_list.push({ name: item.capitalize + ' ' + name , value: value + '-' + item })
        end
      else
        breeds_list.push({ name: name , value: value })
      end
    end
    return breeds_list
  end

  def self.fetch(name=nil)
    name ||= "random"
    DogBreedFetcher.new(name).fetch
  end

private
  def fetch_info
    begin
      JSON.parse(RestClient.get("https://dog.ceo/api/breeds/image/#{ @name }").body)
    rescue Object => e
      default_body
    end
  end

  def default_body
    {
      "status"  => "success",
      "message" => "https://images.dog.ceo/breeds/cattledog-australian/IMG_2432.jpg"
    }
  end
end
